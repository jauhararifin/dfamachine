/*
 * Author
 * Nama : Jauhar Arifin
 * Tanggal : Minggu, 11 September 2016
 * File : main.c
 */

#include "string.h"
#include "boolean.h"
#include "stdio.h"
#include "stdlib.h"

/*
 * uint max_symbol
 * menyatakan jumlah simbol yang boleh digunakan
 */
const uint max_symbol = 1024;

/*
 * uint max_state
 * menyatakan jumlah state yang boleh digunakan
 */
const uint max_state = 1024;

/*
 * uint max_transition
 * menyatakan jumlah transition yang boleh digunakan
 */
const uint max_transition = 1024;

/*
 * uint max_final
 * menyatakan jumlah final state yang boleh digunakan
 */
const uint max_final = 1024;

/*
 * uint max_string
 * menyatakan jumlah simbol yang boleh digunakan dalam membentuk string
 */
const uint max_string = 1024;

/*
 * struct transition
 * tipe data transition menyatakan transition dari suatu state berdasarkan
   simbol tertentu
 * string from : menyatakan state asal
 * string symbol : menyatakan simbol
 * string to : menyatakan state hasil
 */
typedef struct {
	string from,to,symbol;
} transition;

/*
 * isTag
 * mengembalikan nilai true jika s merupakan awalan tag yang valid.
   Tag adalah awalan pada file dfa yang menyatkan awal dari state,
   transition,start,final state, dan alphabet.
 * sebuah tag memiliki karakter ':' di belakangnya selain spasi dan tab
 * string s : string yang ingin dicek apakah tag atau bukan
 * return : boolean yang menyatkaan apakah string s tag atau bukan
 */
boolean isTag(string s) {
	int i;
	for (i = length(s)-1; i >= 0; i--)
		if (s[i] == ' ' || s[i] == '\t')
			continue;
		else
			return s[i] == ':';

	return false;
}

/*
 * getTag
 * mengembalikan nilai tag dari s yaitu dengan menghapus ':' di akhir
   string dan menghapus spasi di awal dan akhir string
 * string s : menyatakn string yang ingin di ambil nilai tagnya
 * return : string yang menyatakan nilai tabnya
 */
string getTag(string s) {
	int i = 0;
	while (i < length(s) && (s[i] == ' ' || s[i] == '\t'))
		i++;

	string tag = 0;
	while (i < length(s) && s[i] != ' ' && s[i] != '\t')
		appendChar(&tag, s[i++]);

	return tag;
}

/*
 * isItem
 * menentukan apakah suatu string merupakan item. Item adalah isi dari tag,
   item dapat berupa simbol,state,maupun transition. Item diawali dengan tab
 * string s : string yang ingin di cek apakah item atau bukan
 */
boolean isItem(string s) {
	int i = 0;
	while (i < length(s) && (s[i] == ' ' || s[i] == '\t'))
		i++;
	return i < length(s);
}

/*
 * getItem
 * mendapatkan nilai item dari string s, yaitu setelah menghilangkan spasi di
   awal dan akhirnya.
 * string s : menyatakan string yang ingin diambil nilai itemnya
 * return : string yang menyatakan nilai item dari string s
 */
string getItem(string s) {
	int i = 0;
	while (i < length(s) && (s[i] == ' ' || s[i] == '\t'))
		i++;

	string item = 0;
	while (i < length(s))
		appendChar(&item, s[i++]);

	return item;
}

/*
 * stringInArray
 * menentukan apakah suatu string berada di dalam array of string
 * string s : string yang ingin dicari
 * string *arr : pointer ke array of string
 * uint n : panjang neff array of string
 * return : boolean yang menyatakan string s ada di array arr
 */
boolean stringInArray(string s, string *arr, uint n) {
	boolean found = false;
	uint i = 0;
	while (!found && i < n)
		found = equals(s, arr[i++]);
	return found;
}

/*
 * isTransition
 * menentukan apakah suatu string merupakan transition. Transition memiliki format
   [from];[simbol];[to]
 * string s : string yang ingin di cek apakah transition atau bukan
 * return : boolean yang menyatakan apakah string s merupakan transition
 */
boolean isTransition(string s) {
	int i,n;
	for (i = n = 0; i < length(s); i++)
		if (s[i] == ';')
			n++;
	return n == 2;
}

/*
 * getTransition
 * mengambil nilai transition dari string s
 * string s : string transition yang dimaksud
 * return : transition yang merupakan transition yang dimaksud string s
 */
transition getTransition(string s) {
	string from = 0;
	string to = 0;
	string symbol = 0;

	int i,j;
	for (i = j = 0; i < length(s); i++) {
		if (s[i] == ';')
			j++;
		else
			switch(j) {
				case 0:
					appendChar(&from, s[i]);
					break;
				case 1:
					appendChar(&symbol, s[i]);
					break;
				case 2:
					appendChar(&to, s[i]);
					break;
			}
	}

	transition d;
	d.from = from;
	d.symbol = symbol;
	d.to = to;

	return d;
}

/*
 * string *arr_symbol
 * menyatakan array untuk menampun simbol
 */
string *arr_symbol;
uint n_symbol = 0;

/*
 * string *arr_state
 * menyatakan array untuk menampun state
 */
string *arr_state;
uint n_state = 0;

/*
 * transition *arr_transition
 * menyatakan array untuk menampun transition
 */
transition *arr_transition;
uint n_transition = 0;

/*
 * string *arr_final
 * menyatakan array untuk menampun final state
 */
string *arr_final;
uint n_final = 0;

/*
 * string *arr_string
 * menyatakan array untuk menampung string
 */
string *arr_string;
uint n_string = 0;

/*
 * string start
 * string yang menyatakan nilai start state
 */
string start = 0;

/*
 * uint start_loc
 * menyatakan pertama line of code dimana start pertama kali
   didefinisikan
 */
uint start_loc = 0;

/*
 * readProgram
 * membaca file program dan mengecek syntax program
 * FILE *fp : file dfa yang ingin dibaca
 * return : boolean yang menyatakan apakah program sukses dibaca
 */
boolean readProgram(FILE *fp) {
	boolean success = true;

	arr_symbol = (string*) malloc(max_symbol * sizeof(string));
	arr_state = (string*) malloc(max_state * sizeof(string));
	arr_transition = (transition*) malloc(max_transition * sizeof(string));
	arr_final = (string*) malloc(max_final * sizeof(string));
	start = 0;
	uint start_loc = 0;
	n_symbol = 0;
	n_state = 0;
	n_transition = 0;
	n_final = 0;

	int linenum = 0;
	string line = 0;
	string tag = 0;
	string item = 0;
	while (freadln(&line, fp) != EOF) {
		linenum++;

		if (isTag(line))
			tag = getTag(line);
		else if (tag == 0)
			fprintf(stderr, "Baris %d tidak didalam tag apapun\n", linenum++), success = false;
		else if (isItem(line)) {
			item = getItem(line);
			if (equals(tag,str("alphabet")))
				arr_symbol[n_symbol++] = item;
			else if (equals(tag,str("state")))
				arr_state[n_state++] = item;
			else if (equals(tag,str("final"))) {
				if (stringInArray(item, arr_state, n_state))
					arr_final[n_final++] = item;
				else
					fprintf(stderr, "Baris %d : State \"%s\" tidak terdefinisi\n", linenum, item), success = false;
			}
			else if (equals(tag,str("start"))) {
				if (start_loc > 0)
					fprintf(stderr, "Baris %d : Start state sudah didefinisikan di baris %d\n", linenum, start_loc), success = false;
				else if (!stringInArray(item, arr_state, n_state))
					fprintf(stderr, "Baris %d : State \"%s\" tidak terdefinisi\n", linenum, item), success = false;
				else {
					start = item;
					start_loc = linenum;
				}
			}
			else if (equals(tag,str("transition"))) {
				if (isTransition(item)) {
					transition d = getTransition(item);

					if (!stringInArray(d.from,arr_state,n_state))
						fprintf(stderr, "Baris %d : State \"%s\" tidak terdefinisi\n", linenum, d.from), success = false;
					else if (!stringInArray(d.symbol,arr_symbol,n_symbol))
						fprintf(stderr, "Baris %d : Simbol \"%s\" tidak terdefinisi\n", linenum, d.symbol), success = false;
					else if (!stringInArray(d.to,arr_state,n_state))
						fprintf(stderr, "Baris %d : State \"%s\" tidak terdefinisi\n", linenum, d.to), success = false;
					else
						arr_transition[n_transition++] = d;
				} else {
					fprintf(stderr, "Baris %d : Format transition tidak valid\n", linenum);
					success = false;
				}
			}
		}
	}
	fclose(fp);

	return success;
}

/*
 * checkDFA
 * menentukan apakah DFA yang dibuat melalui file merupakan
   DFA yang valid. DFA yang valid memiliki minimal satu buah final state dan dari
	 setiap state, memiliki nilai transition untuk setiap simbol
 * return : boolean yang menyatakan apakah DFA valid.
 */
boolean checkDFA() {
	boolean valid = true;
	uint i,j;

	if (n_final <= 0)
		return false;

	for (i = 0; i < n_state; i++)
		for (j = 0; j < n_symbol; j++) {
			uint k = 0;
			boolean temp = false;
			while (!temp && k < n_transition) {
				temp = equals(arr_state[i], arr_transition[k].from) && equals(arr_symbol[j], arr_transition[k].symbol);
				k++;
			}
			if (!temp)
				fprintf(stderr, "Tidak ditemukan simbol \"%s\" untuk state \"%s\"\n", arr_symbol[j], arr_state[i]);
			valid = valid && temp;
		}
	return valid;
}

/*
 * readString
 * membaca string dari stdin
 * return : boolean yang menyatakan apakah string valid
 */
boolean readString() {
	boolean success = true;

	printf("Masukkan string (dapat dipisahkan enter,spasi, atau ';'. Akhiri dengan dua enter atau EOF) :\n");

	arr_string = (string*) malloc(max_string * sizeof(string));
	n_string = 0;

	string s = 0;
	int enter = 1;
	while (enter > 0 && readln(&s) != EOF) {
		if (length(s) <= 0)
			enter--;
		else
			enter = 1;

		string sym = 0;
		int i = 0;
		while (i < length(s)) {
			if (s[i] == ';' || s[i] == ' ') {
				if (length(sym) > 0 && stringInArray(sym, arr_symbol, n_symbol))
					arr_string[n_string++] = sym;
				else if (length(sym) > 0)
					printf("String Error : Simbol \"%s\" tidak terdefinisi\n", sym), success = false;
				sym = 0; resize(&sym, 0);
			} else
				appendChar(&sym, s[i]);

			i++;
		}
		if (length(sym) > 0 && stringInArray(sym, arr_symbol, n_symbol))
			arr_string[n_string++] = sym;
		else if (length(sym) > 0)
			printf("String Error : Simbol \"%s\" tidak terdefinisi\n", sym), success = false;
	}

	return success;
}

/*
 * runProgram
 * menjalankan program berdasarakan informasi DFA dan input
   string
 */
void runProgram() {
	string state = start;

	printf("Start state : \"%s\"\n", state);

	int i = 0;
	while (i < n_string) {
		printf("State \"%s\"\n", state);
		string sym = arr_string[i];
		int j = 0;
		while (!(equals(arr_transition[j].from,state) && equals(arr_transition[j].symbol,sym)) && j < n_transition)
			j++;
		printf("\t %s : \"%s\" --> \"%s\"\n", sym, arr_transition[j].from, arr_transition[j].to);
		state = arr_transition[j].to;
		i++;
	}

	printf("Last state : \"%s\"\n", state);

	printf("\n");
	if (stringInArray(state, arr_final, n_final))
		printf("Hasil : diterima\n");
	else
		printf("Hasil : string ditolak, tidak berakhir di final state\n");
}

int main(int argc, char** argv) {
	// Menentukan file dfa
	string progfile = 0;
	if (argc >= 2)
		progfile = str(argv[1]);
	else {
		fprintf(stderr, "Tidak ada input file dfa. Penggunaan : dfamachine [file_dfa]\n");
		return -1;
	}

	FILE *fp = fopen(progfile, "r");

	// Pengecekan file
	if (fp == 0) {
		fprintf(stderr, "File error : %s\n", progfile);
		return -1;
	}

	// Membaca program dfa dari file
	if (!readProgram(fp)) {
		fprintf(stderr, "Terdapat error dalam membaca program\n");
		return -1;
	}

	printf("Program dari file dfa sukses dibaca\n");

	// Mengecek kevalidan dfa dari file
	if (!checkDFA()) {
		fprintf(stderr, "DFA tidak valid\n");
		return -1;
	}

	printf("DFA yang dimasukkan valid\n");

	// Mulai membaca string dfa dan melakukan pemrosesan
	if (readString()) {
		printf("String yang dimasukkan valid\n\n");
		runProgram();
	} else {
		fprintf(stderr, "Terjadi error dalam membaca string\n");
		return -1;
	}

	return 0;
}
