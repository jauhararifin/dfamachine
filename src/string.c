/*
 * Author
 * Nama : Jauhar Arifin
 * Tanggal : Minggu, 11 September 2016
 * File : string.c
 */

#include "string.h"
#include "boolean.h"
#include "integer.h"
#include "stdlib.h"
#include "stdio.h"

/*
 * Implementasi str pada string.h
 * Tanggal : Minggu, 11 September 2016
 */
string str(char* lit) {
	string baru = 0;
	resize(&baru, 0);

	while (*lit != 0)
		appendChar(&baru, *(lit++));

	return baru;
}

/*
 * Implementasi resize pada string.h
 * Tanggal : Minggu, 11 September 2016
 */
void resize(string *str, uint  sz) {
	void *all = (void*) malloc(sz + 9);

	uint *container = (uint*) all;
	uint *container_length = (uint*) (all + 4);
	string baru = all + 8;

	*container = sz;

	if (*str != 0) {
		uint n = length(*str);
		uint i;
		for (i = 0; i < n && i < sz; i++)
			baru[i] = (*str)[i];
		baru[i] = '\0';
		*container_length = i;
		free(*str-8);
	} else {
		baru[0] = '\0';
		*container_length = 0;
	}

	*str = baru;
}

/*
 * Implementasi length pada string.h
 * Tanggal : Minggu, 11 September 2016
 */
uint length(string str) {
	if (str == 0)
		resize(&str, 0);
	void *p = (void*) str;
	p -= 4;
	uint *q = (uint*) p;
	return *q;
}

/*
 * Implementasi size pada string.h
 * Tanggal : Minggu, 11 September 2016
 */
uint size(string str) {
	if (str == 0)
		resize(&str, 0);
	void *p = (void*) str;
	p -= 8;
	uint *q = (uint*) p;
	return *q;
}

/*
 * Implementasi appendChar pada string.h
 * Tanggal : Minggu, 11 September 2016
 */
void appendChar(string *str, char c) {
	if (*str == 0)
		resize(str, 0);

	if (length(*str) >= size(*str))
		resize(str, ((size(*str) + 1) * 3)/2);

	void *p = *str;
	uint *l = (uint*) (p - 4);
	*(*str + length(*str)) = c;
	(*l)++;
	*(*str + length(*str)) = '\0';
}

/*
 * Implementasi appendString pada string.h
 * Tanggal : Minggu, 11 September 2016
 */
void appendString(string *str, string s) {
	uint i;
	for (i = 0; i < length(s); i++)
		appendChar(str, s[i]);
}

/*
 * Implementasi equals pada string.h
 * Tanggal : Minggu, 11 September 2016
 */
boolean equals(string s1, string s2) {
	if (length(s1) != length(s2))
		return false;

	uint i;
	for (i = 0; i < length(s1); i++)
		if (s1[i] != s2[i])
			return false;

	return true;
}

/*
 * Implementasi readln pada string.h
 * Tanggal : Minggu, 11 September 2016
 */
int readln(string *str) {
	resize(str, 0);

	uint i;
	char x;
	for (i = 0; x != '\n' && x != '\r'; i++) {
		x = (char) getchar();
		if (x != '\n' && x != '\r' && x != EOF)
			appendChar(str, x);
	}

	return x;
}

/*
 * Implementasi freadln pada string.h
 * Tanggal : Minggu, 11 September 2016
 */
int freadln(string *str, FILE *f) {
	resize(str, 0);

	char x;
	for (x = 1; x != '\n' && x != '\r' && x != EOF;) {
		x = (char) fgetc(f);
		if (x != '\n' && x != '\r' && x != EOF)
			appendChar(str, x);
	}

	return x;
}
