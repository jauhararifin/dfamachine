# Tugas I TBFO

Nama	: Jauhar Arifin
NIM		: 13515049

## Cara menjalankan program

1. Pindah ke direktori utama program
2. Pindah ke direktori `bin`
3. Jalankan `./dfamachine [path/to/file.dfa]` pada Linux, `dfamachine.exe [path/to/file.dfa]` pada Windows
4. Untuk tugas ini dapat dijalankan dengan mengetikkan `./dfamachine ../data/marble.dfa`
5. Program akan menjalankan DFA yang terdapat pada file `path/to/file.dfa`
6. Masukkan string input untuk DFA dipisahkan dengan spasi dan akhiri input dengan dua kali menekan enter
7. Hasil state-state yang dilalui serta apakah string input diterima akan ditampilkan di layar

## Struktur direktori

* `bin` berisi file-file binary program
* `data` berisi file-file DFA (*.dfa)
* `src` berisi source code program (*.c dan *.h)
* `doc` berisi laporan dan source code laporan dalam bentuk latex

## Cara build

Catatan: untuk pengumpulan tugas TBFO, file binary program untuk Linux 64-bit serta file PDF laporan sudah disertakan sehingga tidak usah dikompilasi ulang
Program yang dibutuhkan:
* GNU GCC untuk kompilasi program
* PDFLatex untuk pemrosesan laporan

### Build menggunakan GNU Make

1. Pindah ke direktori utama program
2. Jalankan `make` untuk build keseluruhan komponen program (gunakan perintah `make bin` untuk build binary saja, `make doc` untuk build report saja, atau `make clean` untuk membersihkan file-file hasil build)

### Build tanpa menggunakan GNU Make

1. Pindah ke direktori utama program
2. Jalankan `gcc src/main.c src/string.c -o bin/dfamachine` untuk mengompile kode program
3. Jalankan `pdflatex doc/report.tex -output-directory doc` untuk menggenerate laporan

## Format File DFA

alphabet :
	< daftar simbol yang dipisahkan dengan baris baru >

state :
	< daftar state yang dipisahkan dengan baris baru >

final :
	< daftar final state yang dipisahkan dengan baris baru >

start :
	<start state >

transition :
	< daftar fungsi transisi dengan format q_i;a;q_j yang merepresentasikan δ(q_i, a) = q_j >
	< daftar fungsi dipisahkan spasi >
