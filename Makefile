all: build report

build : src/main.c src/string.c
	gcc src/main.c src/string.c -o bin/dfamachine -g

report : doc/report.tex
	pdflatex doc/report.tex -output-directory doc
